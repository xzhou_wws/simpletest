# simpletest
A simple example for Java Spring Interview question.

Trade class are the basic model of each stock transaction

public class Trade {
    String code;
    double price;
    int total;
}

Complete the code (Mainly in ProcessService.java) for a simple restapi service to:
1. Use LoadGeneratorService to send trade transaction to ProcessService.java
2. In ProcessService.java, calculate the total Dollar volume for each trade code (price*total)
3. In ProcessService.java, write a restapi to serve the current total Dollar volume
i.e. when calling /total/bhp
Expecting:
	xxxxx.xx
	
4. Bonus, In ProcessService.java, write a restapi to allow new trade been added.

Please explain how you want to persist the info of the calculation. Any detail about the design is welcome.

